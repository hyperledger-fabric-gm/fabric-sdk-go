/*
Copyright SecureKey Technologies Inc. All Rights Reserved.

SPDX-License-Identifier: Apache-2.0
*/

package pathvar

import (
	"bytes"
	"errors"
	"go/build"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

// CryptoConfigPath is the relative path to the generated crypto config directory
var CryptoConfigPath = "crypto-config"

// Project is the Go project name relative to the Go Path
var Project = "gitee.com/hyperledger-fabric-gm/fabric-sdk-go"

// ProjectPath is the path to the source of fabric-sdk-go
var ProjectPath = ""

var projectPathOnce sync.Once

// GetProjectPath returns the path to the source of fabric-sdk-go. More specifically, this function searches for the
// directory containing the project's go.mod file. This function must only be called from tests.
func GetProjectPath() string {
	projectPathOnce.Do(func() {
		ProjectPath = getProjectPath()
	})
	return ProjectPath
}

func getProjectPath() string {

	// If the current dir is in another module (not root),
	// e.g. when testing within an IDE,
	// the search for the first "go.mod" will hit a non-root file.
	// In that case, set the FABRIC_SDK_GO_PROJECT_PATH environment
	// variable to the correct project root
	val, ok := os.LookupEnv("FABRIC_SDK_GO_PROJECT_PATH")
	if ok {
		return val
	}

	if len(ProjectPath) > 0 {
		return filepath.Clean(ProjectPath)
	}

	pwd, err := os.Getwd()
	if err != nil {
		goPath := goPath()
		return filepath.Join(goPath, "src", Project)
	}
	pwd = filepath.Clean(pwd)

	modDir, err := findParentModule(pwd)
	if err != nil {
		return pwd
	}

	return modDir
}

func findParentModule(wd string) (string, error) {
	for {
		modPath := filepath.Join(wd, "go.mod")
		modExists, err := fileExists(modPath)
		if err != nil {
			return "", err
		}

		if modExists {
			return wd, nil
		}

		pd := filepath.Dir(wd)
		if wd == pd {
			break
		}
		wd = pd
	}
	return "", errors.New("project module was not found")
}

func fileExists(path string) (bool, error) {
	fi, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false, nil
	} else if err != nil {
		return false, err
	}

	if fi.IsDir() {
		return false, nil
	}

	return true, nil
}

// goPath returns the current GOPATH. If the system
// has multiple GOPATHs then the first is used.
func goPath() string {
	gpDefault := build.Default.GOPATH
	gps := filepath.SplitList(gpDefault)

	return gps[0]
}

// Subst replaces instances of '${VARNAME}' (eg ${GOPATH}) with the variable.
// Variables names that are not set by the SDK are replaced with the environment variable.
func Subst(path string) string {
	const (
		sepPrefix = "${"
		sepSuffix = "}"
	)

	splits := strings.Split(path, sepPrefix)

	var buffer bytes.Buffer

	// first split precedes the first sepPrefix so should always be written
	buffer.WriteString(splits[0]) // nolint: gas

	for _, s := range splits[1:] {
		subst, rest := substVar(s, sepPrefix, sepSuffix)
		buffer.WriteString(subst) // nolint: gas
		buffer.WriteString(rest)  // nolint: gas
	}

	return buffer.String()
}

// substVar searches for an instance of a variables name and replaces them with their value.
// The first return value is substituted portion of the string or noMatch if no replacement occurred.
// The second return value is the unconsumed portion of s.
func substVar(s string, noMatch string, sep string) (string, string) {
	endPos := strings.Index(s, sep)
	if endPos == -1 {
		return noMatch, s
	}

	v, ok := lookupVar(s[:endPos])
	if !ok {
		return noMatch, s
	}

	return v, s[endPos+1:]
}

// lookupVar returns the value of the variable.
// The local variable table is consulted first, followed by environment variables.
// Returns false if the variable doesn't exist.
func lookupVar(v string) (string, bool) {
	// TODO: optimize if the number of variable names grows
	switch v {
	case "FABRIC_SDK_GO_PROJECT_PATH":
		return GetProjectPath(), true
	case "GOPATH":
		return goPath(), true
	case "CRYPTOCONFIG_FIXTURES_PATH":
		return CryptoConfigPath, true
	}
	return os.LookupEnv(v)
}
