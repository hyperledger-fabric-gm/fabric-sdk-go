/*
Copyright SecureKey Technologies Inc. All Rights Reserved.

SPDX-License-Identifier: Apache-2.0
*/

package fabsdk

import (
	"gitee.com/hyperledger-fabric-gm/fabric-sdk-go/pkg/core/logging/api"
	"gitee.com/hyperledger-fabric-gm/fabric-sdk-go/pkg/core/logging/modlog"
	sdkApi "gitee.com/hyperledger-fabric-gm/fabric-sdk-go/pkg/fabsdk/api"
	"gitee.com/hyperledger-fabric-gm/fabric-sdk-go/pkg/fabsdk/factory/defcore"
	"gitee.com/hyperledger-fabric-gm/fabric-sdk-go/pkg/fabsdk/factory/defmsp"
	"gitee.com/hyperledger-fabric-gm/fabric-sdk-go/pkg/fabsdk/factory/defsvc"
)

type defPkgSuite struct{}

func (ps *defPkgSuite) Core() (sdkApi.CoreProviderFactory, error) {
	return defcore.NewProviderFactory(), nil
}

func (ps *defPkgSuite) MSP() (sdkApi.MSPProviderFactory, error) {
	return defmsp.NewProviderFactory(), nil
}

func (ps *defPkgSuite) Service() (sdkApi.ServiceProviderFactory, error) {
	return defsvc.NewProviderFactory(), nil
}

func (ps *defPkgSuite) Logger() (api.LoggerProvider, error) {
	return modlog.LoggerProvider(), nil
}
